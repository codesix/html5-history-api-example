$(window).ready(function(){
	bindEvent()
})
	
//change this to match your own server path
var host = "http://codesix.de/html5history/"

//registers click event to the button
function bindEvent(){
	var button = $('#button')
	button.click(function(e){			
		loadContent(button.attr('href'))
		history.pushState(null, null, button.attr('href'))
		return false
	})
	
	$(window).bind('popstate', function() {
		loadContent(location.pathname.split("/").pop())
	})
}

//load the content from the subdirectory
function loadContent(link){
	$.ajax({
		type : 'GET',
		url : host + 'content/' + link,
		success : function(data){
			$('#content').empty().html(data)
			bindEvent()
		},
		error : function(){
			alert('Oooops. Something went wrong.')
		}
	})
}
